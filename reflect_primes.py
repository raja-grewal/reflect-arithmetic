"""
Copyright (C) 2022 - 2024 J. S. Grewal <rg_public@proton.me>

Title:                  reflect_primes.py
Usage:                  python reflect_primes.py

Versioning:
    numpy               2.0
    scipy               1.14

Description:
    Responsible for enumerating prime numbers and repeatedly subtracting each by its
    reflection till either obtaining zero or yielding an infinite loop consisting
    of a finite number of integers. For the second case, we consider the smallest
    positive integer component to be the pattern "break" and determine it to be the
    defining characteristic of the initial prime number regarding the reflection process.

Instructions:
    1. Select parameters. Choose whether to generate a new list of prime numbers or
       elect to use and already generated list.
    2. Running the file will provide live progress in the terminal and save outputs.
    3. If a new pattern break is found, the script will terminate and the value
       must be manually incorporated into the four "^" areas below before restarting.
"""

import os

import numpy as np
from scipy import stats

from utils import cease_subtractions, gen_primes, reflect, test_primes

GENERATE_PRIME_NUMBERS = False
ANALYSE_PRIME_NUMBERS = True

START = 1  # starting number (must be odd)
STOP = 1e8  # ending number (must be > 10,039)

path_data = "./numbers/"
path_analysis = "./summaries/"

if __name__ == "__main__":
    ## TESTS

    START = int(START)
    STOP = int(STOP)

    test_primes(
        GENERATE_PRIME_NUMBERS,
        ANALYSE_PRIME_NUMBERS,
        START,
        STOP,
        path_data,
        path_analysis,
    )

    ## GENERATE PRIME NUMBERS

    if GENERATE_PRIME_NUMBERS:
        gen_primes(START, STOP, path_data)

    ## ANALYSE PRIME NUMBERS

    if ANALYSE_PRIME_NUMBERS:
        prime_numbers = np.load(
            path_data + "prime_numbers_" + "{:.0e}".format(STOP) + ".npy"
        )

        list_primes = []

        known_min_pattern_breaks = [
            2178,
            21978,
            219978,
            2199978,
            11436678,
            13973058,
            21782178,
            21999978,
            114396678,
            139703058,
            217802178,
            219999978,
            # XYZ,
            # ^ add new found minimum positive integer pattern breaks here
        ]

        list_2178 = []
        list_21978 = []
        list_219978 = []
        list_2199978 = []
        list_11436678 = []
        list_13973058 = []
        list_21782178 = []
        list_21999978 = []
        list_114396678 = []
        list_139703058 = []
        list_217802178 = []
        list_219999978 = []
        # list_XYZ = [],
        # ^ add empty lists for new found minimum positive integer pattern breaks here

        list_min_pattern_breaks = [
            list_2178,
            list_21978,
            list_219978,
            list_2199978,
            list_11436678,
            list_13973058,
            list_21782178,
            list_21999978,
            list_114396678,
            list_139703058,
            list_217802178,
            list_219999978,
            # list_XYZ,
            # ^ include (empty) lists for new found minimum positive integer pattern breaks here
        ]

        known_min_pattern_breaks = [int(x) for x in known_min_pattern_breaks]

        for prime in prime_numbers:
            number = prime

            history = []
            history.append(number)

            while number != 0:
                mirror = reflect(number)
                number -= mirror

                history.append(number)

                cease, pattern_break = cease_subtractions(
                    known_min_pattern_breaks, history, STOP
                )

                if cease:
                    print("Prime: {:,} with break at {:,}".format(prime, pattern_break))

                    try:
                        list_primes.append([prime, pattern_break])
                        eval("list_" + str(pattern_break) + ".append(prime)")
                        break

                    except:
                        print("New pattern break discovered:", pattern_break)
                        print(
                            "Must now re-run experiment manually incorporating this value into codebase."
                        )
                        exit()

        # calculate summary statistics
        occurrences = [len(x) for x in list_min_pattern_breaks]
        prime_occurrences = [x / len(prime_numbers) * 100 for x in occurrences]
        rank_occurrences = (
            1 + len(occurrences) - stats.rankdata(occurrences, method="min")
        )

        if not os.path.exists(path_data):
            os.makedirs(path_data)

        # save aggregate pattern break results as a text file
        np.savetxt(
            path_data + "prime_results_" + "{:.0e}".format(STOP) + ".txt",
            list_primes,
            newline="\n",
            fmt="%i",
            header="Prime Numbers: count {} | {} % of {} (inclusive up to {})".format(
                "{:,}".format(len(list_primes)),
                "{:.4f}".format(sum(prime_occurrences)),
                "{:,}".format(len(prime_numbers)),
                "{:.0e}".format(STOP),
            ),
        )

        # fmt: off

        stat_prime = [
            "PRIME SUMMARY STATISTICS" + "  (" + "{:.0e}".format(STOP) + ")",
            "-----------------------------------------------------------------",
            "",
            "Search Start:    {:,}".format(START),
            "Search Stop:     {:,}  (={:.0e})".format(STOP, STOP),
            "",
            "Total Primes:    {:,}".format((len(prime_numbers))),
            "",
            "Pattern Breaks:  {:,}".format(len(list_primes)),
            "",
            "                      Rank       %            Count",
            "",
            "       2,178:         {:02}         {:04.1f}         {:,}".format(rank_occurrences[0], prime_occurrences[0], occurrences[0]),
            "      21,978:         {:02}         {:04.1f}         {:,}".format(rank_occurrences[1], prime_occurrences[1], occurrences[1]),
            "     219,978:         {:02}         {:04.1f}         {:,}".format(rank_occurrences[2], prime_occurrences[2], occurrences[2]),
            "   2,199,978:         {:02}         {:04.1f}         {:,}".format(rank_occurrences[3], prime_occurrences[3], occurrences[3]),
            "  11,436,678:         {:02}         {:04.1f}         {:,}".format(rank_occurrences[4], prime_occurrences[4], occurrences[4]),
            "  13,973,058:         {:02}         {:04.1f}         {:,}".format(rank_occurrences[5], prime_occurrences[5], occurrences[5]),
            "  21,782,178:         {:02}         {:04.1f}         {:,}".format(rank_occurrences[6], prime_occurrences[6], occurrences[6]),
            "  21,999,978:         {:02}         {:04.1f}         {:,}".format(rank_occurrences[7], prime_occurrences[7], occurrences[7]),
            " 114,396,678:         {:02}         {:04.1f}         {:,}".format(rank_occurrences[8], prime_occurrences[8], occurrences[8]),
            " 139,703,058:         {:02}         {:04.1f}         {:,}".format(rank_occurrences[9], prime_occurrences[9], occurrences[9]),
            " 217,802,178:         {:02}         {:04.1f}         {:,}".format(rank_occurrences[10], prime_occurrences[10], occurrences[10]),
            " 219,999,978:         {:02}         {:04.1f}         {:,}".format(rank_occurrences[11], prime_occurrences[11], occurrences[11]),
            # ^ add entries for new found minimum positive integer pattern breaks here
            "",
            "   TOTAL SUM:                    {:04.1f}         {:,}".format(sum(prime_occurrences), sum(occurrences)),
        ]

        # fmt: on

        if not os.path.exists(path_analysis):
            os.makedirs(path_analysis)

        # create summary text file for printed overall results
        with open(
            path_analysis + "summary_primes_" + "{:.0e}".format(STOP) + ".txt", "w"
        ) as summary:
            for text in stat_prime:
                summary.write(text)
                summary.write("\n")
