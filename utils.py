"""
Copyright (C) 2022 - 2024 J. S. Grewal <rg_public@proton.me>

Title:                  utils.py

Versioning:
    numpy               2.0
    scipy               1.14

Description:
    Collection of tools required for conducting tests, generating prime numbers,
    and the subtraction process for reflection.
"""

import os
from collections import Counter
from os import PathLike
from typing import List, Tuple, Union

import numpy as np


def cease_subtractions(
    known_min_pattern_breaks: List[int],
    history: List[int],
    stop: int,
    threshold: Union[float, int] = 1e8,
) -> Tuple[bool, Union[None, int]]:
    """
    Automatic cessation of repeat subtraction process and identification of
    they key minimum positive integer pattern break.

    For larger numbers, the pattern is automatically recognised and the key
    minimum positive integer pattern breaks is derived algorithmically. For smaller
    numbers, a list of known pattern breaks are used to recognise the pattern.

    If the pattern break is new, script will terminate and and the value must be
    manually incorporated into the codebase before restarting the script.

    Note, we opt for using "stop" as opposed to "history[-1]" regarding determining
    the "threshold" criteria as it is faster over the long-run when the numbers
    become larger.

    Parameters:
        known_min_pattern_breaks: existing pattern breaks
        history: entire history of number reflection process
        stop: largest number to be analysed
        threshold: limit at which direct pattern break identification stops

    Returns:
        cease: whether to stop reflection process
        pattern_break: minimum positive integer pattern break
    """
    # utilise general pattern recognition for larger numbers
    if stop > threshold:
        frequency = Counter(history)

        # select lowest positive integer if a repeating pattern is observed
        if any(count == 3 for count in frequency.values()):
            for k, v in list(frequency.items()):
                if v == 1 or int(k) < 0:
                    del frequency[k]

            pattern_break = min(frequency.keys())

            return True, pattern_break

        else:
            return False, None

    # use simple direct cessation rule for known behaviour of smaller numbers
    else:
        if history[-1] in known_min_pattern_breaks:
            return True, history[-1]
        else:
            return False, None


def gen_primes(start: int, stop: int, path_data: Union[str, bytes, PathLike]) -> None:
    """
    Generate prime numbers through analysing only odd numbers. Saves enumerated
    list as both a text file and NumPy (np.int64) file.

    Parameters:
        start: beginning odd number
        stop: final number
        path_data: location to save generated primes
    """
    primes = []

    actual_start = start

    if actual_start == 1:
        primes.append(2)
        actual_start = 3

    # only analyse odd numbers
    for number in range(actual_start, stop + 1, 2):
        for divisor in range(3, number, 2):
            if (number % divisor) == 0:
                # print("Composite: {:,}".format(number))
                break
        else:
            primes.append(int(number))
            print("Prime: {:,}".format(number))

    if len(primes) == 0:
        print("No prime numbers within range of {:,} -> {:,}.".format(start, stop))
        exit()

    if not os.path.exists(path_data):
        os.makedirs(path_data)

    np.savetxt(
        path_data + "prime_numbers_" + "{:.0e}".format(stop) + ".txt",
        primes,
        newline="\n",
        fmt="%i",
    )

    array = np.array(primes, dtype=np.int64)

    np.save(path_data + "prime_numbers_" + "{:.0e}".format(stop) + ".npy", array)


def reflect(number: int) -> int:
    """
    Reverses the provided integer by first converting it to a string and then
    reflecting the digits.

    Parameters:
        number: provided integer

    Returns:
        mirror: reversed provided integer
    """
    mirror = str(number)[::-1]

    if mirror[-1] == "-":
        return -int(mirror[:-1])

    return int(mirror)


def test_numbers(
    start: int,
    stop: int,
    interval: int,
    path_data: Union[str, bytes, PathLike],
    path_analysis: Union[str, bytes, PathLike],
) -> None:
    """
    Test input parameters for all number experiments.
    """
    assert start >= 1, "START must be greater than or equal to 1"
    assert stop > start, "STOP must be greater than START"
    assert stop > interval > 0, "INTERVAL must be greater than zero and less than STOP"

    assert int(str(stop).replace("0", "")) <= 9, "STOP must have only one leading digit"

    assert isinstance(
        path_data, Union[str, bytes, PathLike]
    ), "path_data must be directory path"
    assert isinstance(
        path_analysis, Union[str, bytes, PathLike]
    ), "path_analysis must be directory path"


def test_primes(
    gen_prime_numbers: bool,
    analyse_prime_numbers: bool,
    start: int,
    stop: int,
    path_data: Union[str, bytes, PathLike],
    path_analysis: Union[str, bytes, PathLike],
) -> None:
    """
    Test input parameters and existing databases for all prime number experiments.
    """
    assert isinstance(gen_prime_numbers, bool)
    assert isinstance(analyse_prime_numbers, bool)

    assert start >= 1, "START must be greater than or equal to 1"
    assert (start % 2) == 1, "START must be a odd number"
    assert (stop > 10039) and (
        stop > start
    ), "STOP must be greater than both 10,039 and START"

    assert int(str(stop).replace("0", "")) <= 9, "STOP must have only one leading digit"

    assert isinstance(
        path_data, Union[str, bytes, PathLike]
    ), "path_data must be directory path"
    assert isinstance(
        path_analysis, Union[str, bytes, PathLike]
    ), "path_analysis must be directory path"

    # check for the existence of the required prime number database
    if not gen_prime_numbers:
        assert os.path.isfile(
            path_data + "prime_numbers_" + "{:.0e}".format(stop) + ".npy"
        ), "missing required '{}' file (must first be generated)".format(
            "prime_numbers_" + "{:.0e}".format(stop) + ".npy"
        )
