# Reflect Arithmetic

[![License](https://img.shields.io/badge/License-MIT-green)](https://codeberg.org/raja-grewal/reflect-arithmetic/src/branch/main/LICENSE)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://pre-commit.com/)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)

The purpose of this repository is to empirically generate a list so called 'pattern breaks'. These are defined as numbers wherein repeat subtraction of an initial number by its reflection eventually leads to an infinite loop where the smallest positive integer contained within the loop is considered the 'pattern break'. All other initial numbers collapse to zero via this process.

A step-by-step example of this reflection process can be highlighted using 2,046 where

1. $2,046 - 6,402 = -4,356$
2. $-4,356 + 6,534 = 2178$
3. $2,178 - 8,712 = -6,534$
4. $-6,534 + 4,356 = -2,178$
5. $-2,178 + 8,712 = 6,534$
6. $6,534 - 4,356 = 2,178$
7. $...$

and so we can observe the infinite loop and then define 2,178 to be the 'pattern break'.

Alternatively, a 6-step example next using 5,946 reveals its eventual collapse to zero since

1. $5,946 - 6,495 = -549$
2. $549 - 945 = 396$
3. $396 - 693 = -297$
4. $-297 + 792 = 495$
5. $495 -594 = -99$
6. $-99 + 99 = 0$.

Given that all composite numbers are composed of prime number factors, we focus our analysis primarily on prime numbers. Due to limitations with total RAM capacity and CPU bottleneck (single-thread), we limit our empirical search to prime numbers under 10 billion. Note, while GPU-based computations are much faster, they are similarly untenable due to the very substantial VRAM requirements.

Detailed summary statistics are available in the `./summaries` directory.

## Key Findings (Draft)

Overall, we currently find that over 60% of all prime numbers less than 10 billion do not collapse to zero. Instead, the reflection process generates infinite loops with 12 pattern breaks that do not occur in equal frequency.

The summary statistics reveal that the proportion of prime numbers that do not collapse to zero appear to monotonically and asymptotically rise as the amount of prime numbers tested increase. The results also highlight that any given pattern break only occurs when testing a prime number at least one order of magnitude greater, implying that the only way to empirically discover more pattern breaks is by testing larger prime numbers.

For the 12 identified pattern breaks, they can be clearly grouped according to the following four unique sets:

$A = \{2178, 21978, 219978, 2199978, 21999978,219999978\}$

$B = \{11436678,114396678\}$

$C = \{13973058, 139703058\}$

$D = \{21782178, 217802178\}$

We can then also perform prime factorisations for each pattern break:

#### Set $A$:

$2,178 = 2 \times 3^2 \times 11^2$

$21,978 = 2 \times 3^3 \times 11 \times 37$

$219,978 = 2 \times 3^2 \times 11^2 \times 101$

$2,199,978 = 2 \times 3^2 \times 11 \times 41 \times 271$

$21,999,978 = 2 \times 3^3 \times 7 \times 11^2 \times 13 \times 37$

$219,999,978 = 2 \times 3^2 \times 11 \times 239 \times 4,649$

#### Set $B$:

$11,436,678 = 2 \times 3^2 \times 11^2 \times 59 \times 89$

$114,396,678 = 2 \times 3^3 \times 11 \times 192,587$

#### Set $C$:

$13,973,058 = 2 \times 3^2 \times 11 \times 70,571$

$139,703,058 = 2 \times 3^2 \times 11 \times 23 \times 30,677$

#### Set $D$:

$21,782,178 = 2 \times 3^2 \times 11^2 \times 73 \times 137$

$217,802,178 = 2 \times 3^2 \times 11^3 \times 9,091$

Notice that each of the pattern breaks (left side of the equations) are strictly even, they all numerically end with 8, and that the sum of each of their digits are divisible by 9.

Let us now define the prime factors that are shared across all 12 pattern breaks with

$F_p = (2, 3, 11)$

and the shared common factors with

$F_q = (2, 3, 6, 9, 11, 18, 22, 33, 66, 99, 198)$

where clearly $\text{dim}(F_p) \leq \text{dim}(F_q)$. Next, define the largest prime factor $P$ as

$P \equiv \max(F_p) = 11$

and largest common factor $Q$ as

$Q \equiv \max(F_q) = 2 \times 3^2 \times 11 = 60 + 138 = 198$

which is also divisible by the sum of its digits (18). Recognise also its distinct connection to both the keeper of time (PN) and the timely period of X.

This enables us to separately display the previous factorisations in terms of both $P$ and $Q$:

| Set $A$ | Set $B$ | Set $C$ | Set $D$ |
| ----------- | ----------- | ----------- | ----------- |
| $2,178 = P \times 198$ |  |  |  |
| $21,978 = P \times 1,998$ |  |  |  |
| $219,978 = P \times 19,998$ |  |  |  |
| $2,199,978 = P \times 199,998$ |  |  |  |
| $21,999,978 = P \times 1,999,998$ | $11,436,678 = P \times 1,039,698$ | $13,973,058 = P \times 1,270,278$ | $21,782,178 = P \times 1,980,198$ |
| $219,999,978 = P \times 19,999,998$ | $114,396,678 = P \times 10,399,698$ | $139,703,058 = P \times 12,700,278$ | $217,802,178 = P \times 19,800,198$ |

| Set $A$ | Set $B$ | Set $C$ | Set $D$ |
| ----------- | ----------- | ----------- | ----------- |
| $2,178 = Q \times 11$ |  |  |  |
| $21,978 = Q \times 111$ |  |  |  |
| $219,978 = Q \times 1,111$ |  |  |  |
| $2,199,978 = Q \times 11,111$ |  |  |  |
| $21,999,978 = Q \times 111,111$ | $11,436,678 = Q \times 57,761$ | $13,973,058 = Q \times 70,571$ | $21,782,178 = Q \times 110,011$ |
| $219,999,978 = Q \times 1,111,111$ | $114,396,678 = Q \times 577,761$ | $139,703,058 = Q \times 705,571$ | $217,802,178 = Q \times 1,100,011$ |

Overall, from these empirically derived sets, additional pattern breaks for each of the known sets can by hypothesised and tested based on the clearly observed patterns. Discovery of entirely new sets would likely involve directly searching for them empirically.

Regarding predicting new pattern breaks (left side of the equations) for each of the four known sets, this may involve the following insertions; $A$. 9’s, $B$. 9’s, $C$. 0’s, and $D$. 0’s. All these insertions occur at the midpoint of each pattern break.

Similarly, in terms of the multipliers of $P$ (right side of the equations) this may involve the midpoint insertions; $A$. 9’s, $B$. 9’s, $C$. 0’s, and $D$. 0’s. Finally, in terms of the multipliers of $Q$ this may involve the midpoint insertions; $A$. 1’s, $B$. 7’s, $C$. 5’s, and $D$. 0’s.

## Usage

Accessing the code involves having already installed [Git LFS](https://git-lfs.github.com/) and then running the following commands:
```
git clone https://codeberg.org/raja-grewal/reflect-arithmetic.git

cd reflect-arithmetic
```

Next enable Git LFS and then both `fetch` and `checkout` the most up-to-date existing data:
```
git lfs install

git lfs pull
```

Install all required packages (ideally in a virtual environment) without dependencies using:
```
pip install setuptools pip --upgrade

pip install numpy scipy
```

Optionally, install and enable the use of [pre-commit](https://pre-commit.com/) for making contributions:
```
pip install pre-commit

pre-commit install
```

Inside both `reflect_primes.py` and `reflect_numbers.py`, various parameters can be tuned as desired. A NumPy array of already generated prime numbers up to 100 million (1e+08) is provided and is directly usable by default.

Running the default experiment for prime numbers involves:
```
python reflect_primes.py
```

Note, all other enumerations of prime numbers will have to be manually generated prior to running their respective experiments. Alternatively, all data is available upon request.

Copyright (C) 2022 - 2024 J. S. Grewal (rg_public@proton.me).
