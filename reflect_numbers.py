"""
Copyright (C) 2022 - 2024 J. S. Grewal <rg_public@proton.me>

Title:                  reflect_numbers.py
Usage:                  python reflect_numbers.py

Versioning:
    numpy               2.0
    scipy               1.14

Description:
    Responsible for repeatedly subtracting a number by its reflection till either
    obtaining zero or yielding an infinite loop consisting of a finite number of
    integers. For the second case, we consider the smallest positive integer component
    to be the pattern "break" and determine it to be the defining characteristic of
    the initial number regarding the reflection process.

Instructions:
    1. Select parameters.
    2. Running the file will provide live progress in the terminal and save outputs.
    3. If a new pattern break is found, the script will terminate and the value
       must be manually incorporated into the four "^" areas below restarting.
"""

import os

import numpy as np
from scipy import stats

from utils import cease_subtractions, reflect, test_numbers

START = 1  # starting number
STOP = 1e8  # ending number
INTERVAL = 1  # step size between numbers

path_data = "./numbers/"
path_analysis = "./summaries/"

if __name__ == "__main__":
    ## TESTS

    START = int(START)
    STOP = int(STOP)
    INTERVAL = int(INTERVAL)

    test_numbers(START, STOP, INTERVAL, path_data, path_analysis)

    ## ANALYSE ALL NUMBERS

    known_min_pattern_breaks = [
        2178,
        21978,
        219978,
        2199978,
        11436678,
        13973058,
        21782178,
        21999978,
        114396678,
        139703058,
        217802178,
        219999978,
        # XYZ,
        # ^ add new found minimum positive integer pattern breaks here
    ]

    list_2178 = []
    list_21978 = []
    list_219978 = []
    list_2199978 = []
    list_11436678 = []
    list_13973058 = []
    list_21782178 = []
    list_21999978 = []
    list_114396678 = []
    list_139703058 = []
    list_217802178 = []
    list_219999978 = []
    # list_XYZ = [],
    # ^ add empty lists for new found minimum positive integer pattern breaks here

    list_min_pattern_breaks = [
        list_2178,
        list_21978,
        list_219978,
        list_2199978,
        list_11436678,
        list_13973058,
        list_21782178,
        list_21999978,
        list_114396678,
        list_139703058,
        list_217802178,
        list_219999978,
        # list_XYZ,
        # ^ include (empty) lists for new found minimum positive integer pattern breaks here
    ]

    known_min_pattern_breaks = [int(value) for value in known_min_pattern_breaks]

    for initial_number in range(START, int(STOP + 1), INTERVAL):
        number = initial_number

        history = []
        history.append(number)

        while number != 0:
            mirror = reflect(number)
            number -= mirror

            history.append(number)

            cease, pattern_break = cease_subtractions(
                known_min_pattern_breaks,
                history,
                STOP,
            )

            if cease:
                print(
                    "Number: {:,} with break at {:,}".format(
                        initial_number, pattern_break
                    )
                )

                try:
                    eval("list_" + str(pattern_break) + ".append(initial_number)")
                    break

                except:
                    print("New pattern break discovered:", pattern_break)
                    print(
                        "Must now re-run experiment manually incorporating this value into codebase."
                    )
                    exit()

    # calculate summary statistics
    occurrences = [len(x) for x in list_min_pattern_breaks]
    number_occurrences = [x / STOP * 100 for x in occurrences]
    rank_occurrences = 1 + len(occurrences) - stats.rankdata(occurrences, method="min")

    if not os.path.exists(path_data):
        os.makedirs(path_data)

    # save results for each pattern break as a text file
    for item in range(0, len(known_min_pattern_breaks)):
        np.savetxt(
            path_data
            + str(known_min_pattern_breaks[item])
            + "_"
            + "{:.0e}".format(STOP)
            + ".txt",
            eval("list_" + str(known_min_pattern_breaks[item])),
            newline="\n",
            fmt="%i",
            header="Number: {} with count {} | {} % of {} (={})".format(
                "{:,}".format(known_min_pattern_breaks[item]),
                "{:,}".format(len(eval("list_" + str(known_min_pattern_breaks[item])))),
                "{:.4f}".format(number_occurrences[item]),
                "{:,}".format(STOP),
                "{:.0e}".format(STOP),
            ),
        )

    # fmt: off

    stat = [
        "SUMMARY STATISTICS" + "  (" + "{:.0e}".format(STOP) + ")",
        "-----------------------------------------------------------------",
        "",
        "Start:           {:,}".format(START),
        "Stop:            {:,}  (={:.0e})".format(STOP, STOP),
        "Step Size:       {:,}".format(INTERVAL),
        "",
        "Pattern Breaks:  {:,}".format(sum(occurrences)),
        "",
        "                      Rank       %            Count",
        "",
        "       2,178:         {:02}         {:04.1f}         {:,}".format(rank_occurrences[0], number_occurrences[0], occurrences[0]),
        "      21,978:         {:02}         {:04.1f}         {:,}".format(rank_occurrences[1], number_occurrences[1], occurrences[1]),
        "     219,978:         {:02}         {:04.1f}         {:,}".format(rank_occurrences[2], number_occurrences[2], occurrences[2]),
        "   2,199,978:         {:02}         {:04.1f}         {:,}".format(rank_occurrences[3], number_occurrences[3], occurrences[3]),
        "  11,436,678:         {:02}         {:04.1f}         {:,}".format(rank_occurrences[4], number_occurrences[4], occurrences[4]),
        "  13,973,058:         {:02}         {:04.1f}         {:,}".format(rank_occurrences[5], number_occurrences[5], occurrences[5]),
        "  21,782,178:         {:02}         {:04.1f}         {:,}".format(rank_occurrences[6], number_occurrences[6], occurrences[6]),
        "  21,999,978:         {:02}         {:04.1f}         {:,}".format(rank_occurrences[7], number_occurrences[7], occurrences[7]),
        " 114,396,678:         {:02}         {:04.1f}         {:,}".format(rank_occurrences[8], number_occurrences[8], occurrences[8]),
        " 139,703,058:         {:02}         {:04.1f}         {:,}".format(rank_occurrences[9], number_occurrences[9], occurrences[9]),
        " 217,802,178:         {:02}         {:04.1f}         {:,}".format(rank_occurrences[10], number_occurrences[10], occurrences[10]),
        " 219,999,978:         {:02}         {:04.1f}         {:,}".format(rank_occurrences[11], number_occurrences[11], occurrences[11]),
        # ^ add entries for new found minimum positive integer pattern breaks here
        "",
        "   TOTAL SUM:                    {:04.1f}         {:,}".format(sum(number_occurrences), sum(occurrences)),

    ]

    # fmt: on

    if not os.path.exists(path_analysis):
        os.makedirs(path_analysis)

    # create summary text file for printed overall results
    with open(
        path_analysis + "summary_" + "{:.0e}".format(STOP) + ".txt", "w"
    ) as summary:
        for text in stat:
            summary.write(text)
            summary.write("\n")
